# Demo
This repo demonstrates the use of [rules_buildstream](https://gitlab.com/celduin/buildstream-bazel/rules_buildstream)
and [bazel-toolchains-fdsdk](https://gitlab.com/celduin/buildstream-bazel/bazel-toolchains-fdsdk) by building and
defining a toolchains for native and cross-compilation on [minimal-rootfs](https://gitlab.com/celduin/buildstream-bazel/minimal-rootfs).

This demo is being run in the CI cross-compiling abseil-hello on an x86_64 runner with the aarch64 toolchain
from bazel-toolchains-fdsdk for an aarch64 runner 
